package net.borlcand.wot.api;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Tanks extends GenericApi {

    public Tanks(){
        this.method_block = "tanks";
    }
    
}
