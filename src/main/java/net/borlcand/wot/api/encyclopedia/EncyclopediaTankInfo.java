package net.borlcand.wot.api.encyclopedia;

import java.util.Arrays;
import net.borlcand.wot.api.genericapi.Language;
import org.apache.http.message.BasicNameValuePair;
import java.util.List;
import net.borlcand.wot.api.Encyclopedia;
import net.borlcand.wot.ulil.CsvUtil;

public class EncyclopediaTankInfo extends Encyclopedia {

    public EncyclopediaTankInfo(Language lang, String fields, List<String> tank_id) {
        this.method_name = "tankinfo";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (tank_id != null) {
            this.get_params.add(new BasicNameValuePair("tank_id", CsvUtil.lineFormat(tank_id.toArray())));
        }

        callApi();
    }

    public EncyclopediaTankInfo(String fields, List<String> tank_id) {
        this(null, fields, tank_id);
    }

    public EncyclopediaTankInfo(List<String> tank_id) {
        this(null, null, tank_id);
    }

    public EncyclopediaTankInfo(Language lang, List<String> tank_id) {
        this(lang, null, tank_id);
    }

    public EncyclopediaTankInfo(String fields, String tank_id) {
        this(null, fields, Arrays.asList(tank_id));
    }

    public EncyclopediaTankInfo(String tank_id) {
        this(null, null, Arrays.asList(tank_id));
    }

    public EncyclopediaTankInfo(Language lang, String tank_id) {
        this(lang, null, Arrays.asList(tank_id));
    }
}
