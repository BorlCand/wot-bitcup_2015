package net.borlcand.wot.api.encyclopedia;

import net.borlcand.wot.api.genericapi.Language;
import net.borlcand.wot.api.Encyclopedia;

public class EncyclopediaTanks extends Encyclopedia {

    public EncyclopediaTanks(Language lang, String fields) {
        this.method_name = "tanks";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }

        callApi();
    }
    
    public EncyclopediaTanks(String fields) {
        this(null, fields);
    }

    public EncyclopediaTanks(Language lang) {
        this(lang, null);
    }

    public EncyclopediaTanks() {
        this(null, null);
    }
}
