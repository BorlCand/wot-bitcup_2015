package net.borlcand.wot.api;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Encyclopedia extends GenericApi {

    public Encyclopedia(){
        this.method_block = "encyclopedia";
    }
    
}
