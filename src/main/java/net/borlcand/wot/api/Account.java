package net.borlcand.wot.api;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Account extends GenericApi {

    public Account(){
        this.method_block = "account";
    }
    
}
