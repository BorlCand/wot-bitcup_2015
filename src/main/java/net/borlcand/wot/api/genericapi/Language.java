package net.borlcand.wot.api.genericapi;

public enum Language {

    EN("en"),
    RU("ru"),
    PL("pl"),
    DE("de"),
    FR("fr"),
    ES("es"),
    ZHCN("zh-cn"),
    TR("tr"),
    CS("cs"),
    TH("th"),
    VI("vi"),
    KO("ko"),
    DEFAULT("ru");

    private final String lang;

    Language(String lang) {
        this.lang = lang;
    }

    public String getValue() {
        if (this == null) {
            return RU.getValue();
        }
        return lang;
    }

    public static Language fromValue(String lang) {
        for (Language l : values()) {
            if (l.getValue().equals(lang)) {
                return l;
            }
        }
        return null;
    }
}
