package net.borlcand.wot.api.account;

public enum SearchType {
    
    EXACT("exact"),
    STARTSWITH("startswith");
    
    private final String type;

    SearchType(String type) {
        this.type = type;
    }

    public String getValue() {
        if (this == null){
            return STARTSWITH.getValue();
        }
        return type;
    }
    
    public static SearchType fromValue(String type) {
        for (SearchType l: values()) {
            if (l.getValue().equals(type)) {
                return l;
            }
        }
        return null;
    }
    
}
