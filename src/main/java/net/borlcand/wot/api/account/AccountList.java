package net.borlcand.wot.api.account;

import net.borlcand.wot.api.Account;
import net.borlcand.wot.api.genericapi.Language;
import org.apache.http.message.BasicNameValuePair;

public class AccountList extends Account {

    public AccountList(Language lang, String fields, SearchType type, String search, Object limit) {
        this.method_name = "list";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (type != null) {
            this.get_params.add(new BasicNameValuePair("type", type.getValue()));
        }
        if (search != null) {
            this.get_params.add(new BasicNameValuePair("search", search));
        }
        if (limit != null) {
            this.get_params.add(new BasicNameValuePair("limit", String.valueOf(limit)));
        }

        callApi();
    }

    public AccountList(String fields, String search, Object limit) {
        this(null, fields, null, search, limit);
    }

    public AccountList(Language lang, String fields, SearchType type, String search) {
        this(lang, fields, type, search, null);
    }

    public AccountList(String fields, SearchType type, String search) {
        this(null, fields, type, search, null);
    }

    public AccountList(SearchType type, String search) {
        this(null, null, type, search, null);
    }

    public AccountList(String search, Object limit) {
        this(null, null, null, search, limit);
    }

    public AccountList(String search) {
        this(null, null, null, search, null);
    }
}
