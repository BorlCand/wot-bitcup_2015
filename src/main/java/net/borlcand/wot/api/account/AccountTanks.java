package net.borlcand.wot.api.account;

import java.util.Arrays;
import java.util.List;
import net.borlcand.wot.api.Account;
import net.borlcand.wot.api.genericapi.Language;
import net.borlcand.wot.ulil.CsvUtil;
import org.apache.http.message.BasicNameValuePair;

public class AccountTanks extends Account {

    public AccountTanks(Language lang, String fields, List<String> account_id, List<String> tank_id) {
        this.method_name = "tanks";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (account_id != null) {
            this.get_params.add(new BasicNameValuePair("account_id", CsvUtil.lineFormat(account_id.toArray())));
        }
        if (tank_id != null) {
            this.get_params.add(new BasicNameValuePair("tank_id", CsvUtil.lineFormat(tank_id.toArray())));
        }

        callApi();
    }
    
    public AccountTanks(String fields, List<String> account_id, List<String> tank_id) {
        this(null, fields, account_id, tank_id);
    }

    public AccountTanks(String fields, List<String> account_id) {
        this(null, fields, account_id, null);
    }

    public AccountTanks(List<String> account_id) {
        this(null, null, account_id, null);
    }
    
    public AccountTanks(String fields, String account_id, String tank_id) {
        this(null, fields, Arrays.asList(account_id), Arrays.asList(tank_id));
    }

    public AccountTanks(String fields, String account_id) {
        this(null, fields, Arrays.asList(account_id), null);
    }

    public AccountTanks(String account_id) {
        this(null, null, Arrays.asList(account_id), null);
    }
}
