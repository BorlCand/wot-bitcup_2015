package net.borlcand.wot.api.account;

import java.util.Arrays;
import java.util.List;
import net.borlcand.wot.api.Account;
import net.borlcand.wot.api.genericapi.Language;
import net.borlcand.wot.ulil.CsvUtil;
import org.apache.http.message.BasicNameValuePair;

public class AccountInfo extends Account {

    public AccountInfo(Language lang, String fields, List<String> account_id) {
        this.method_name = "info";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (account_id != null) {
            this.get_params.add(new BasicNameValuePair("account_id", CsvUtil.lineFormat(account_id.toArray())));
        }

        callApi();
    }

    public AccountInfo(String fields, List<String> account_id) {
        this(null, fields, account_id);
    }

    public AccountInfo(List<String> account_id) {
        this(null, null, account_id);
    }

    public AccountInfo(Language lang, List<String> account_id) {
        this(lang, null, account_id);
    }

    public AccountInfo(String fields, String account_id) {
        this(null, fields, Arrays.asList(account_id));
    }

    public AccountInfo(String account_id) {
        this(null, null, Arrays.asList(account_id));
    }

    public AccountInfo(Language lang, String account_id) {
        this(lang, null, Arrays.asList(account_id));
    }
}
