package net.borlcand.wot.api;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Ratings extends GenericApi {

    public Ratings(){
        this.method_block = "ratings";
    }
    
}
