package net.borlcand.wot.api.tanks;

import net.borlcand.wot.api.genericapi.Language;
import org.apache.http.message.BasicNameValuePair;
import net.borlcand.wot.api.Tanks;

public class TanksStats extends Tanks {

    public TanksStats(Language lang, String fields, String account_id, String tank_id, String in_garage) {
        this.method_name = "stats";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (account_id != null) {
            this.get_params.add(new BasicNameValuePair("account_id", account_id));
        }
        if (tank_id != null) {
            this.get_params.add(new BasicNameValuePair("tank_id", tank_id));
        }
        if (in_garage != null) {
            this.get_params.add(new BasicNameValuePair("in_garage", in_garage));
        }

        callApi();
    }

    public TanksStats(Language lang, String fields, String account_id, String tank_id) {
        this(lang, fields, account_id, tank_id, null);
    }

    public TanksStats(String fields, String account_id, String tank_id) {
        this(null, fields, account_id, tank_id, null);
    }

    public TanksStats(String fields, String account_id) {
        this(null, fields, account_id, null, null);
    }

    public TanksStats(String account_id) {
        this(null, null, account_id, null, null);
    }
}
