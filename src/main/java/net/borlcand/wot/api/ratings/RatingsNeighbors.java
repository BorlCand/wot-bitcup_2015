package net.borlcand.wot.api.ratings;

import net.borlcand.wot.api.Ratings;
import net.borlcand.wot.api.genericapi.Language;
import org.apache.http.message.BasicNameValuePair;

public class RatingsNeighbors extends Ratings {

    public RatingsNeighbors(Language lang, String fields, BattleType battle_type, String type, String date, String account_id, String rank_field, Object limit) {
        this.method_name = "neighbors";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (battle_type != null) {
            this.get_params.add(new BasicNameValuePair("battle_type", battle_type.getValue()));
        }
        if (type != null) {
            this.get_params.add(new BasicNameValuePair("type", type));
        }
        if (date != null) {
            this.get_params.add(new BasicNameValuePair("date", date));
        }
        if (account_id != null) {
            this.get_params.add(new BasicNameValuePair("account_id", account_id));
        }
        if (rank_field != null) {
            this.get_params.add(new BasicNameValuePair("rank_field", rank_field));
        }
        if (limit != null) {
            this.get_params.add(new BasicNameValuePair("limit", String.valueOf(limit)));
        }

        callApi();
    }

    public RatingsNeighbors(String fields, BattleType battle_type, String type, String date, String account_id, String rank_field, Object limit) {
        this(null, fields, battle_type, type, date, account_id, rank_field, limit);
    }

    public RatingsNeighbors(String fields, BattleType battle_type, String type, String account_id, String rank_field, Object limit) {
        this(null, fields, battle_type, type, null, account_id, rank_field, limit);
    }

    public RatingsNeighbors(String fields, BattleType battle_type, String type, String account_id, String rank_field) {
        this(null, fields, battle_type, type, null, account_id, rank_field, null);
    }

    public RatingsNeighbors(String fields, String type, String account_id, String rank_field, Object limit) {
        this(null, fields, null, type, null, account_id, rank_field, limit);
    }

    public RatingsNeighbors(String fields, String type, String account_id, String rank_field) {
        this(null, fields, null, type, null, account_id, rank_field, null);
    }

    public RatingsNeighbors(String type, String account_id, String rank_field) {
        this(null, null, null, type, null, account_id, rank_field, null);
    }
}
