package net.borlcand.wot.api.ratings;

public enum BattleType {
    
    COMPANY("company"),
    RANDOM("random"),
    TEAM("team"),
    DEFAULT("default");
    
    private final String type;

    BattleType(String type) {
        this.type = type;
    }

    public String getValue() {
        if (this == null){
            return DEFAULT.getValue();
        }
        return type;
    }
    
    public static BattleType fromValue(String type) {
        for (BattleType l: values()) {
            if (l.getValue().equals(type)) {
                return l;
            }
        }
        return null;
    }
    
}
