package net.borlcand.wot.api;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Clan extends GenericApi {

    public Clan(){
        this.method_block = "clan";
    }
    
}
