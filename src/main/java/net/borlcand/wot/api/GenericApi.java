package net.borlcand.wot.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import net.borlcand.wot.api.genericapi.Language;
import net.borlcand.wot.connection.HttpConnection;
import net.borlcand.wot.ulil.JsonUtil;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

@Accessors(chain = true)
public class GenericApi {

    private String application_id = "61f1cf4ed2d937a924bf22f2720dfec4";

    @Getter
    @Setter
    private String access_token = "";

    @Getter
    @Setter
    protected String fields = "";

    @Getter
    protected Language language = Language.DEFAULT;

    /**
     * http(s)://<server>/<API_name>/<method block>/<method name>/?<get params>
     * <server> — URI игрового сервера на соответствующем кластере
     * <API_name> — версия API
     * <method block> — название группы методов
     * <method name> — название метода
     * <get params> — параметры метода GET для запроса
     */
    private String server = "api.worldoftanks.ru";
    private String API_name = "wot";
    protected String method_block = "";
    protected String method_name = "";
    protected List<NameValuePair> get_params = new ArrayList<>();

    @Getter
    protected JsonElement data = null,
            error = null;

    protected GenericApi() {
        get_params.add(new BasicNameValuePair("application_id", this.application_id));
        get_params.add(new BasicNameValuePair("access_token", this.access_token));
    }

    protected void callApi() {
        List<NameValuePair> params = new ArrayList<>(get_params);

        params.add(new BasicNameValuePair("language", language.getValue()));
        params.add(new BasicNameValuePair("fields", fields));

        String apiUrl = String.format("https://%s/%s/%s/%s/", server, API_name, method_block, method_name);

        try {
            URI uri = new URIBuilder(apiUrl).addParameters(params).build();
            
            JsonObject response = JsonUtil.parseJson(HttpConnection.readFromUrl(uri));
            
            if (response.get("status").getAsString().equalsIgnoreCase("ok")) {
                JsonElement temp = response.get("data");
                
                data = temp;
            } else {
                if (response.get("status").getAsString().equalsIgnoreCase("error")) {
                    error = response.getAsJsonObject("error");
                } else {
                    data = null;
                }
            }
        } catch (URISyntaxException | IOException ex) {
            data = null;
        }
    }

    public JsonElement getAsJsonElement() {
        return data;
    }

    public JsonObject getAsJsonObject() {
        return (data != null && data.isJsonObject()) ? data.getAsJsonObject() : null;
    }

    public JsonArray getAsJsonArray() {
        return (data != null && data.isJsonArray()) ? data.getAsJsonArray() : null;
    }

    public JsonPrimitive getAsJsonPrimitive() {
        return (data != null && data.isJsonPrimitive()) ? data.getAsJsonPrimitive() : null;
    }
}
