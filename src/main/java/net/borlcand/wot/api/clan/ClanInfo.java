package net.borlcand.wot.api.clan;

import java.util.Arrays;
import net.borlcand.wot.api.genericapi.Language;
import org.apache.http.message.BasicNameValuePair;
import java.util.List;
import net.borlcand.wot.api.Clan;
import net.borlcand.wot.ulil.CsvUtil;

public class ClanInfo extends Clan {

    public ClanInfo(Language lang, String fields, List<String> clan_id) {
        this.method_name = "info";

        if (lang != null) {
            this.language = lang;
        }
        if (fields != null) {
            this.fields = fields;
        }
        if (clan_id != null) {
            this.get_params.add(new BasicNameValuePair("clan_id", CsvUtil.lineFormat(clan_id.toArray())));
        }

        callApi();
    }

    public ClanInfo(String fields, List<String> clan_id) {
        this(null, fields, clan_id);
    }

    public ClanInfo(List<String> clan_id) {
        this(null, null, clan_id);
    }

    public ClanInfo(Language lang, List<String> clan_id) {
        this(lang, null, clan_id);
    }

    public ClanInfo(String fields, String clan_id) {
        this(null, fields, Arrays.asList(clan_id));
    }

    public ClanInfo(String clan_id) {
        this(null, null, Arrays.asList(clan_id));
    }

    public ClanInfo(Language lang, String clan_id) {
        this(lang, null, Arrays.asList(clan_id));
    }
}
