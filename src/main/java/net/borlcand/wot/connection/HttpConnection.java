package net.borlcand.wot.connection;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpConnection {

    public static String readFromUrl(URI uri) throws MalformedURLException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(uri);
        CloseableHttpResponse response = httpClient.execute(httpGet);

        String result = null;
        try {
            HttpEntity entity = response.getEntity();

            result = EntityUtils.toString(entity, "UTF-8");
        } finally {
            response.close();
        }

        return result;
    }

    public static String readFromUrl(String uri) throws MalformedURLException, IOException, URISyntaxException {

        URI url = new URI(uri);

        return readFromUrl(url);
    }

}
