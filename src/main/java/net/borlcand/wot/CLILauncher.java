package net.borlcand.wot;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import net.borlcand.wot.commandexecutor.FindPlayerCommandExecutor;
import net.borlcand.wot.commandexecutor.FindRivalsCommandExecutor;
import net.borlcand.wot.commandexecutor.StatisticsCommandExecutor;
import net.borlcand.wot.commandexecutor.SuggestTeamCommandExecutor;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class CLILauncher {

    private static Option findPlayer = new Option("fp", "find-player", true, "Finds player by his name");
    private static Option findRivals = new Option("fr", "find-rivals", true, "Finds rivals for frags count by player ID");
    private static Option statistics = new Option("stats", "statistics", true, "Creates CSV-table with statistics");
    private static Option suggestTeam = new Option("st", "suggest-team", true, "Tries to suggest teams");

    private static OptionGroup mainCommand = new OptionGroup();
    public static Options options;

    public static void main(String[] args) {
        findPlayer.setArgName("player_name");
        findRivals.setArgName("player_id");
        statistics.setArgName("player-id_list");
        suggestTeam.setArgName("clan-id> <tank-type_list");

        suggestTeam.setArgs(2);

        mainCommand
                .addOption(findPlayer)
                .addOption(findRivals)
                .addOption(statistics)
                .addOption(suggestTeam)
                .setRequired(true);

        options = new Options().addOptionGroup(mainCommand);

        CommandLineParser cmdLineParser = new PosixParser();
        CommandLine commandLine;
        
        try {
            System.setOut(new PrintStream(new BufferedOutputStream(System.out), true, "UTF-8"));

            commandLine = cmdLineParser.parse(options, args);

            if (commandLine.hasOption(findPlayer.getOpt())) {
                //System.out.println(new List("account_id,nickname", SearchType.EXACT, commandLine.getOptionValue(findPlayer.getOpt())).getAsJsonArray());
                new FindPlayerCommandExecutor(commandLine.getOptionValue(findPlayer.getOpt()));
            }

            if (commandLine.hasOption(findRivals.getOpt())) {
                new FindRivalsCommandExecutor(commandLine.getOptionValue(findRivals.getOpt()));
            }

            if (commandLine.hasOption(statistics.getOpt())) {
                new StatisticsCommandExecutor(commandLine.getOptionValue(statistics.getOpt()));
            }

            if (commandLine.hasOption(suggestTeam.getOpt())) {
                if (commandLine.getOptionValues(suggestTeam.getOpt()).length != 2) {
                    throw new ParseException("Not enough required arguments");
                }

                new SuggestTeamCommandExecutor(commandLine.getOptionValues(suggestTeam.getOpt()));
            }

        } catch (ParseException ex) {
            printHelp(options, 80, "Options:", "\n-- Powered by BorlCand Java API Implementation --", 2, 3, true, System.out);
        } catch (UnsupportedEncodingException ex) {
            System.out.println("unknown_error");
        } finally {
            System.out.close();
        }
    }

    public static void printHelp(
            final Options options,
            final int printedRowWidth,
            final String header,
            final String footer,
            final int spacesBeforeOption,
            final int spacesBeforeOptionDescription,
            final boolean displayUsage,
            final OutputStream out) {
        final String commandLineSyntax = "java -jar program.jar";
        final PrintWriter writer = new PrintWriter(out);
        final HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(
                writer,
                printedRowWidth,
                commandLineSyntax,
                header,
                options,
                spacesBeforeOption,
                spacesBeforeOptionDescription,
                footer,
                displayUsage);
        writer.flush();
    }
}
