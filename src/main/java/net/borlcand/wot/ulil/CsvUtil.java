package net.borlcand.wot.ulil;

public class CsvUtil {
    
    
    public static String lineFormat(Object[] object){
        String result = "";
        
        for (int i = 0; i<object.length;i++){
            Object obj = object[i];
            
            result += String.valueOf(obj);
            if (i+1 != object.length){
                result += ",";
            }
        }
        
        return result;
    }
}
