package net.borlcand.wot.ulil;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.lang.String;
import net.borlcand.wot.connection.HttpConnection;

public class JsonUtil {

    public static JsonObject parseJson(String json) throws IOException {
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(json);
        JsonObject rootobj = root.getAsJsonObject();

        return rootobj;
    }

    public static void collectAllKeys(List keys, Object o, Boolean preserveOrder) {
        Collection values = null;
        if (o instanceof Map) {
            Map map = (Map) o;
            keys.addAll(map.keySet()); // collect keys at current level in hierarchy
            values = map.values();
        } else if (o instanceof Collection) {
            values = (Collection) o;
        } else // nothing further to collect keys from
        {
            return;
        }

        for (Object value : values) {
            collectAllKeys(keys, value, preserveOrder);
        }

        if (preserveOrder) {
            Collections.reverse(keys);
        }
    }

    public static List collectAllKeys(Object o, Boolean preserveOrder) {
        List keys = new ArrayList();

        collectAllKeys(keys, o, preserveOrder);

        return keys;
    }

    public static List collectAllKeys(Object o) {
        return collectAllKeys(o, false);
    }

    public static void collectAllValuesFromKeys(List keys, List values, Object o, Boolean preserveOrder) {
        Collection keyvalues = null;
        if (o instanceof Map) {
            Map map = (Map) o;
            values.addAll(map.values());
            keyvalues = map.values();
        } else if (o instanceof Collection) {
            keyvalues = (Collection) o;
        } else {
            return;
        }

        for (Object value : keyvalues) {
            collectAllValuesFromKeys(keys, values, value, preserveOrder);
        }

        if (preserveOrder) {
            Collections.reverse(keys);
        }
    }

    public static List collectAllValuesFromKeys(List keys, Object o, Boolean preserveOrder) {
        List values = new ArrayList();

        collectAllValuesFromKeys(keys, values, o, preserveOrder);

        return values;
    }

    public static List collectAllValuesFromKeys(List keys, Object o) {
        return collectAllKeys(o, false);
    }

    public static List<List<String>> collectAllKeyValuePairs(JsonObject o, Boolean preserveOrder) {
        List<List<String>> result = new ArrayList<>();
        result.add(new ArrayList<String>());
        result.add(new ArrayList<String>());
        
        for (Map.Entry<String, JsonElement> entrySet : o.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue().getAsString();

            result.get(0).add(key);
            result.get(1).add(value);
        }
       
        if (preserveOrder){
            for (List<String> list : result) {
                Collections.reverse(list);
            }
        }
        
        return result;
    }
    
    public static List<List<String>> collectAllKeyValuePairs(JsonObject o) {
        return collectAllKeyValuePairs(o, false);
    }
}
