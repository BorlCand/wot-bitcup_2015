package net.borlcand.wot.commandexecutor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import net.borlcand.wot.api.account.AccountInfo;
import net.borlcand.wot.api.account.AccountTanks;
import net.borlcand.wot.api.encyclopedia.EncyclopediaTankInfo;
import net.borlcand.wot.ulil.CsvUtil;

public class StatisticsCommandExecutor {

    public StatisticsCommandExecutor(String account_id) {
        LinkedHashMap<String, String> players = new LinkedHashMap<>();

        LinkedHashSet<String> playerIdsTemp = new LinkedHashSet<>(Arrays.asList(account_id.split(",")));
        int apiQueryCounter = (int) Math.ceil((double) playerIdsTemp.size() / 100);

        for (int i = 0; i < apiQueryCounter; i++) {
            JsonObject accountTemp = new AccountInfo("nickname", new ArrayList<>(playerIdsTemp).subList(i * 100, Math.min(playerIdsTemp.size(), (i + 1) * 100))).getAsJsonObject();

            if (accountTemp != null) {
                for (Map.Entry<String, JsonElement> account : accountTemp.entrySet()) {
                    if (account != null && !account.getValue().isJsonNull()) {
                        players.put(account.getKey(), account.getValue().getAsJsonObject().get("nickname").getAsString());
                    }
                }
            }
        }

        if (players.isEmpty()) {
            System.out.println("unknown_error");
            return;
        }

        LinkedHashMap<String, LinkedHashMap<String, String>> playersTanks = new LinkedHashMap<>();
        LinkedHashMap<String, String> allTanks = new LinkedHashMap<>();

        apiQueryCounter = (int) Math.ceil((double) players.keySet().size() / 100);

        for (int i = 0; i < apiQueryCounter; i++) {
            JsonObject allPlayersTanks = new AccountTanks("tank_id,statistics.wins,statistics.battles", new ArrayList<>(players.keySet()).subList(i * 100, Math.min(players.keySet().size(), (i + 1) * 100))).getAsJsonObject();

            for (Map.Entry<String, JsonElement> player : allPlayersTanks.entrySet()) {
                JsonArray allPlayerTanks = player.getValue().getAsJsonArray();
                LinkedHashMap<String, String> playerTanks = new LinkedHashMap<>();

                for (JsonElement entry : allPlayerTanks) {
                    String tank_id = entry.getAsJsonObject().get("tank_id").getAsString();
                    double wins = entry.getAsJsonObject().getAsJsonObject("statistics").get("wins").getAsDouble(),
                            battles = entry.getAsJsonObject().getAsJsonObject("statistics").get("battles").getAsDouble();

                    if (battles == 0) {
                        continue;
                    }

                    String winrate = String.valueOf((int) (wins / battles * 100));

                    playerTanks.put(tank_id, winrate);
                    allTanks.put(tank_id, null);
                }

                playersTanks.put(player.getKey(), playerTanks);
            }
        }

        JsonObject playedTanksNames = new EncyclopediaTankInfo("localized_name", new ArrayList<>(allTanks.keySet())).getAsJsonObject();

        for (Map.Entry<String, String> entry : allTanks.entrySet()) {
            String key = entry.getKey();

            String tankName = playedTanksNames.getAsJsonObject(key).get("localized_name").getAsString().replaceAll(",", ".");

            entry.setValue(tankName);
        }

        System.out.println(CsvUtil.lineFormat(new String[]{"account_id", "nickname"}) + "," + CsvUtil.lineFormat(allTanks.values().toArray()));

        for (Map.Entry<String, String> player : players.entrySet()) {
            String playerId = player.getKey();

            List<String> printValues = new ArrayList<>();

            printValues.addAll(Arrays.asList(playerId, player.getValue()));

            for (Map.Entry<String, String> playedTank : allTanks.entrySet()) {
                String tankWinRate;

                if (playersTanks.get(playerId).containsKey(playedTank.getKey())) {
                    tankWinRate = playersTanks.get(playerId).get(playedTank.getKey()) + "%";
                } else {
                    tankWinRate = "-";
                }

                printValues.add(tankWinRate);
            }

            System.out.println(CsvUtil.lineFormat(printValues.toArray()));
        }
    }
}
