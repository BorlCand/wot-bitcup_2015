package net.borlcand.wot.commandexecutor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.borlcand.wot.api.account.AccountInfo;
import net.borlcand.wot.api.ratings.RatingsNeighbors;
import net.borlcand.wot.ulil.CsvUtil;

public class FindRivalsCommandExecutor {

    public FindRivalsCommandExecutor(String account_id) {

        JsonArray playerRivals = new RatingsNeighbors("account_id,frags_count", "28", account_id, "frags_count", 10).getAsJsonArray();

        if (playerRivals == null) {
            System.out.println("unknown_error");
            return;
        }

        LinkedHashMap<String, List<String>> players = new LinkedHashMap<>();

        for (JsonElement playerRival : playerRivals) {

            String playerId = playerRival.getAsJsonObject().get("account_id").getAsString();
            String fragsCount = playerRival.getAsJsonObject().getAsJsonObject("frags_count").get("value").getAsString();

            players.put(playerId, Arrays.asList(playerId, null, fragsCount));
        }

        JsonObject playerNames = new AccountInfo("nickname", new ArrayList<>(players.keySet())).getAsJsonObject();

        System.out.println(CsvUtil.lineFormat(new String[]{"account_id", "nickname", "frags_count"}));
        for (Map.Entry<String, List<String>> entrySet : players.entrySet()) {
            String key = entrySet.getKey();
            List<String> value = entrySet.getValue();
            value.set(1, playerNames.get(key).getAsJsonObject().get("nickname").getAsString());

            System.out.println(CsvUtil.lineFormat(value.toArray()));
        }
    }
}
