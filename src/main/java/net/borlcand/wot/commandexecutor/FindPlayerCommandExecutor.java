package net.borlcand.wot.commandexecutor;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import net.borlcand.wot.api.account.SearchType;
import net.borlcand.wot.ulil.CsvUtil;
import net.borlcand.wot.ulil.JsonUtil;

public class FindPlayerCommandExecutor {

    public FindPlayerCommandExecutor(String player) {
        JsonArray jsonPlayer = new net.borlcand.wot.api.account.AccountList("account_id,nickname", SearchType.EXACT, player).getAsJsonArray();

        if (jsonPlayer == null) {
            System.out.println("unknown_error");
            return;
        }

        if (jsonPlayer.size() == 0) {
            System.out.println("not_found");
            return;
        }

        List<List<String>> result = new ArrayList<>();
        result.add(new ArrayList<String>());
        result.add(new ArrayList<String>());

        JsonObject playerObject = jsonPlayer.get(0).getAsJsonObject();

        result = JsonUtil.collectAllKeyValuePairs(playerObject, true);

        for (List<String> printResult : result) {
            System.out.println(CsvUtil.lineFormat(printResult.toArray()));
        }

    }
}
