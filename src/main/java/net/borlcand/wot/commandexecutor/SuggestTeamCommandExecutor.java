package net.borlcand.wot.commandexecutor;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.borlcand.wot.api.account.AccountTanks;
import net.borlcand.wot.api.clan.ClanInfo;
import net.borlcand.wot.api.encyclopedia.EncyclopediaTanks;
import net.borlcand.wot.ulil.CsvUtil;

public class SuggestTeamCommandExecutor {

    public SuggestTeamCommandExecutor(String[] args) {

        String clanId = args[0];
        List<String> suggestedTankTypes = new ArrayList<>(Arrays.asList(args[1].split(",")));

        JsonObject clanInfo = new ClanInfo("members.account_name", clanId).getAsJsonObject();

        if (clanInfo == null || clanInfo.get(clanId).isJsonNull()) {
            System.out.println("unknown_error");
            return;
        }

        clanInfo = clanInfo.getAsJsonObject(clanId);

        LinkedHashMap<String, String> players = new LinkedHashMap<>();
        LinkedHashMap<String, LinkedHashMap<String, Double>> playersTanks = new LinkedHashMap<>();
        LinkedHashMap<String, LinkedHashMap<String, String>> allTanks = new LinkedHashMap<>();

        for (Map.Entry<String, JsonElement> entry : clanInfo.getAsJsonObject("members").entrySet()) {
            String account_id = entry.getKey();

            String account_name = entry.getValue().getAsJsonObject().get("account_name").getAsString();

            players.put(account_id, account_name);
        }

        for (Map.Entry<String, JsonElement> entry : new EncyclopediaTanks("name_i18n,level,type").getAsJsonObject().entrySet()) {
            String typeLevel = String.format("%s-%s", entry.getValue().getAsJsonObject().get("type").getAsString(), entry.getValue().getAsJsonObject().get("level").getAsString());

            if (!allTanks.containsKey(typeLevel)) {
                allTanks.put(typeLevel, new LinkedHashMap<String, String>());
            }

            allTanks.get(typeLevel).put(entry.getKey(), entry.getValue().getAsJsonObject().get("name_i18n").getAsString().replaceAll(",", "."));
        }

        Set<String> allTanksIds = new HashSet<>();
        for (String suggestedTankType : suggestedTankTypes) {
            LinkedHashMap<String, String> typeTanks = allTanks.get(suggestedTankType);

            if (typeTanks == null) {
                System.out.println("impossible");
                return;
            }
            
            allTanksIds.addAll(typeTanks.keySet());
        }
        
        
        //Wanted to get only tanks that i need, but stupid API limit on tank_id ruin it :(. Nowhere to insert allTanksIds
        JsonObject playersStats = new AccountTanks("statistics.wins,statistics.battles,tank_id", new ArrayList<>(players.keySet())).getAsJsonObject();

        for (Map.Entry<String, JsonElement> entry : playersStats.entrySet()) {
            if (!playersTanks.containsKey(entry.getKey())) {
                playersTanks.put(entry.getKey(), new LinkedHashMap<String, Double>());
            }

            for (JsonElement tankStat : entry.getValue().getAsJsonArray()) {
                String tank_id = tankStat.getAsJsonObject().get("tank_id").getAsString();
                double wins = tankStat.getAsJsonObject().getAsJsonObject("statistics").get("wins").getAsDouble(),
                        battles = tankStat.getAsJsonObject().getAsJsonObject("statistics").get("battles").getAsDouble();

                if (battles >= 100) {
                    double winrate = wins / battles * 100;

                    playersTanks.get(entry.getKey()).put(tank_id, winrate);
                }
            }
        }

        System.out.println(CsvUtil.lineFormat(new String[]{"account_id", "nickname", "tank", "wins_percentage"}));

        Set<String> usedPlayers = new HashSet<>();
        for (String suggestedTankType : suggestedTankTypes) {

            LinkedHashMap<String, Object> chosen = new LinkedHashMap<>();
            LinkedHashMap<String, String> typeTanks = allTanks.get(suggestedTankType);
            chosen.put("account_id", "-1");
            chosen.put("tank_id", "-1");
            chosen.put("winrate", "-1");

            for (Map.Entry<String, LinkedHashMap<String, Double>> player : playersTanks.entrySet()) {

                if (usedPlayers.contains(player.getKey())) {
                    continue;
                }

                for (Map.Entry<String, Double> tank : player.getValue().entrySet()) {

                    if (!typeTanks.containsKey(tank.getKey())) {
                        continue;
                    }

                    if (tank.getValue() > Double.valueOf(chosen.get("winrate").toString())) {
                        chosen.put("account_id", player.getKey());
                        chosen.put("tank_id", tank.getKey());
                        chosen.put("winrate", tank.getValue());
                    }
                }
            }

            if (chosen.isEmpty() || chosen.get("account_id").toString().equalsIgnoreCase("-1")) {
                System.out.println("impossible");
                return;
            }

            usedPlayers.add(chosen.get("account_id").toString());

            String account_id = chosen.get("account_id").toString();
            String account_name = players.get(chosen.get("account_id").toString());
            String tank_name = allTanks.get(suggestedTankType).get(chosen.get("tank_id").toString());
            String winrate = Double.valueOf(chosen.get("winrate").toString()).intValue() + "%";

            System.out.println(CsvUtil.lineFormat(new String[]{account_id, account_name, tank_name, winrate}));
        }
    }
}
